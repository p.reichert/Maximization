# Maximization

Julia interface to different exixting optimizers.



## Files

* **`Maximization.jl`**: Function `Maximization` to be called as an interface to different exixting optimizers

* **`TransPar.jl`**: Parameter transformation from one- and two-sided bounded domains to the real axis.

* **`Maximization_Test.jl`**: Script to test the interface`.




## Functions

### Maximization.jl

Mandatory arguments:

* `f`: function taking a parameter vector `par` as its argument and returning a real value
       (e.g. log posterior for estimating the maximum posterior parameter values)

* `par_ini`: `Float64` vector of initial parameter values

* `parnames`: `String` vector of names of parameters `par_ini`

Optional arguments (for defaults see source code):

* `Trans`: vector with elements of type `ParTrans` containing parameter transformations to be applied for unconstrained inference of transformed parameters

* `OptAlg`: optimization algorithm, currently available choices:

  `"NelderMead"`: Nelder-Mead algorithm from  `Optim.jl`
  
  `"LBFGS"`: LBFGS algorithm from  `Optim.jl`
  
* `AutoDiff`: Algorithm for automatic differentiation. Currently `ForwardDiff` or `Zygote`

* `maxiter`: maximum number of iterations to be performed

* `verbose`: logical variable indicating whether some information should be provided about the optimization process

Return value:

* 2-column matrix with parameter names and final parameter values

