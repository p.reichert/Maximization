# ============================================================================
##
## File: Maximization_Test.jl
##
## Test script for interface function to optimization algorithms
##
## creation:          August 6, 2023 -- Peter Reichert
## last modification: August 8, 2023 -- Peter Reichert
##
## contact:           peter.reichert@emeriti.eawag.ch
##
## ============================================================================


# Activate, instatiate and load packages:
# =======================================

import Pkg
Pkg.activate(".")
Pkg.instantiate()

include("Maximization.jl")


# Set algorithmic parameters:
# ===========================

AutoDiff = ForwardDiff

OptAlgs = ["NelderMead","LBFGS"]

maxiters = 100


# Define initial parameter values and log density:
# ================================================

par      = [1.0,1.0,1.0,1.0]
parnames = ["p1","p2","p3","p4"]

means    = [1.0,2.0,3.0,4.0]
sds      = [0.2,0.4,0.6,0.8]

# Trans = missing

Trans = Array{TransPar}(undef,4)
Trans[1] = TransParUnbounded()
Trans[2] = TransParLowerBound(0.0)
Trans[3] = TransParUpperBound(5.0)
Trans[4] = TransParInterval(0.0,5.0)

function logdensity(par,means,sds)
   return - 0.5*sum(((par.-means)./sds).^2)
end

logdensity(par) = logdensity(par,means,sds)


# Sample and plot for all algorithms:
# ===================================

for OptAlg in OptAlgs

   par_opt = Maximization(logdensity,par,parnames;
                          Trans    = Trans,
                          AutoDiff = AutoDiff,
                          OptAlg   = OptAlg,
                          maxiters = maxiters)

   display(par_opt)
   
end


